# docker-bak

#### 介绍
docker学习中编写的一些东西。配置了php5.6 和php7.1环境，都加上了xdebug扩展

#### 使用说明

1. dockerfile里面php5.6的Dockerfile，添加了xdebug\redis\memcache 等基础扩展
2. php5.6-docker 里面是编译好的php5.6 docker-compose.yml
3. php7.1-docker 里面是编译好的php7.1.30 docker-compose.yml，这里还编入了xunsearch
